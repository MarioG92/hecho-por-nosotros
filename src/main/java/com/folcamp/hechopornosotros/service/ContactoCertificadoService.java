package com.folcamp.hechopornosotros.service;

import com.folcamp.hechopornosotros.models.dto.ContactCertifNewDTO;
import com.folcamp.hechopornosotros.models.dto.ContactoNewDTO;
import com.folcamp.hechopornosotros.models.repositories.EmprendimientoRepository;
import com.folcamp.hechopornosotros.util.UploadBeanImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactoCertificadoService {
    @Autowired
    private ContactoService contactoService;
    @Autowired
    private EmprendimientoRepository emprendimientoRepository;
    @Autowired
    private StorageService storageService;

//    public ResponseEntity<ContactCertifNewDTO> create(ContactCertifNewDTO contactCertifNewDTO) {
//
//        if (!emprendimientoRepository.existsById(contactCertifNewDTO.getEmprendimientoId())) {
//            throw new RuntimeException("No se encontro emprendimiento");
//        }
//        contactoService.createContacto(contactCertifNewDTO.getEmprendimientoId(), contactCertifNewDTO.getContactoNewDTO());
//
//        List<UploadBeanImage> uploadBeanImages = contactCertifNewDTO.getUploadBeanImages();
//
//        for(UploadBeanImage uploadBeanImage: uploadBeanImages){
//            storageService.storeImageEmprendimiento(uploadBeanImage);
//        }
//
//        return new ResponseEntity<>(contactCertifNewDTO, HttpStatus.OK);
//    }
}
